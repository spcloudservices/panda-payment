import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SqpApiModule } from '@squarepanda/sqp-api';
import { SqpCoreModule } from '@squarepanda/sqp-core';
import { SqpUiModule } from '@squarepanda/sqp-ui';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './_reducers';
import { environment } from '@env/environment';

const routes = [
  { path: '', pathMatch: 'full', redirectTo: 'main' },
  {
    path: 'main',
    loadChildren: 'app/main/main.module#MainModule'
  }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes, { useHash: true }),
    SqpApiModule.forRoot(environment),
    SqpCoreModule.forRoot(environment, routes),
    SqpUiModule.forRoot('teacher'),
    StoreModule.forRoot(reducers, { metaReducers })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
