import { Component } from '@angular/core';
import { SqpSdkTranslateService, SqpSdkCountriesService } from '@squarepanda/sqp-sdk';
import { environment } from '@env/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private sdkCountries: SqpSdkCountriesService,
    private sdkTranslate: SqpSdkTranslateService
  ) {
    const locale = localStorage.getItem('locale') || environment.locale;
    this.sdkTranslate.setLanguage(locale).subscribe();
    this.sdkCountries.get().subscribe();
  }
}
