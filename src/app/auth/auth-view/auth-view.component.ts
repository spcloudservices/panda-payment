import { Component, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { SqpIdentityService } from '@squarepanda/sqp-reducer';
import { tap, filter } from 'rxjs/operators';
import { AlertService } from '@squarepanda/sqp-core';
import { SqpHttpErrorsService } from '@squarepanda/sqp-api';

@Component({
  selector: 'app-auth-view',
  templateUrl: './auth-view.component.html',
  styleUrls: ['./auth-view.component.scss']
})
export class AuthViewComponent implements OnInit {

  logo;
  language = environment.locale;
  options = {
    tos: environment.CON_termsContentCode,
    pp: environment.CON_privacyContentCode
  };

  constructor(
    private alert: AlertService,
    private sqpErrors: SqpHttpErrorsService,
    private sqpIdentity: SqpIdentityService
  ) {

    this.sqpErrors.get().pipe(
      tap((message) => this.alert.info({ type: 'fail', message }))
    ).subscribe();

    this.sqpIdentity.get().pipe(
      filter((identity) => identity && identity.termsOfUse && identity.privacyPolicy),
      tap((identity) => {
        this.options.tos = identity.termsOfUse.contentCode;
        this.options.pp = identity.privacyPolicy.contentCode;
      })
    ).subscribe();
  }

  ngOnInit() {
  }

}
