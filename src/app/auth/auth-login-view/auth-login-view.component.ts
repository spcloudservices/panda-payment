import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RouteService, StorageService } from '@squarepanda/sqp-core';
import { switchMap, catchError, tap, filter } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { SqpCountriesService } from '@squarepanda/sqp-reducer';
import { AuthService } from '@shared/_services/auth.service';
import { SQP_LANGUAGES } from '@squarepanda/sqp-constants';
import { environment } from '@env/environment';
import { SqpSdkTranslateService, SqpSdkAuthService } from '@squarepanda/sqp-sdk';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-auth-login-view',
  templateUrl: './auth-login-view.component.html',
  styleUrls: ['./auth-login-view.component.scss']
})
export class AuthLoginViewComponent {

  languageForm;
  form;
  pressed;
  SQP_LANGUAGES = SQP_LANGUAGES;
  loginMethodForm;
  countries$;

  constructor(
    private fb: FormBuilder,
    private route: RouteService,
    private storage: StorageService,
    private authService: AuthService,
    private sqpAuth: SqpSdkAuthService,
    private activatedRoute: ActivatedRoute,
    private sqpCountries: SqpCountriesService,
    private sdkTranslate: SqpSdkTranslateService
  ) {
    this.countries$ = this.sqpCountries.get();

    this.form = this.fb.group({
      password: [null, [Validators.required]],
      phoneNumber: this.fb.group({
        countryCode: [environment.countryCode],
        number: []
      })
    });

    this.activatedRoute.queryParams.pipe(
      filter((params: any) => params.countryCode && params.number),
      tap((params) => {
        this.form.controls['phoneNumber'].controls['countryCode'].patchValue(params.countryCode);
        this.form.controls['phoneNumber'].controls['number'].patchValue(params.number);
      })
    ).subscribe();

    const locale = localStorage.getItem('locale') || environment.locale;
    this.languageForm = this.fb.group({
      locale: [locale]
    });

    this.languageForm.get('locale').valueChanges.pipe(
      tap((v: any) => localStorage.setItem('locale', v)),
      switchMap((v) => this.sdkTranslate.setLanguage(v))
    ).subscribe();

    this.storage.clear();
  }

  submit() {
    // if (!this.pressed) {
    //   this.pressed = true;
    //   this.login(this.form.value).pipe(
    //     catchError((err) => {
    //       this.pressed = false;
    //       return throwError(err);
    //     }),
    //     tap(console.log)
    //     // switchMap((tokenInfo) => this.authService.continue(tokenInfo))
    //   ).subscribe();
    // }
  }

  private login(params) {
    return this.sqpAuth.loginV2(params);
  }

}
