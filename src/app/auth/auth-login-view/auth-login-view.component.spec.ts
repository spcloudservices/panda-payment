import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthLoginViewComponent } from './auth-login-view.component';
import { SqpCommonTestingModule, SqpApiTestingModule } from '@testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuthLoginViewComponent', () => {
  let component: AuthLoginViewComponent;
  let fixture: ComponentFixture<AuthLoginViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        SqpCommonTestingModule,
        RouterTestingModule,
        SqpApiTestingModule
      ],
      declarations: [ AuthLoginViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthLoginViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
