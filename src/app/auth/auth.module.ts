import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthViewComponent } from './auth-view/auth-view.component';
import { AuthLoginViewComponent } from './auth-login-view/auth-login-view.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { AuthCheckViewComponent } from './auth-check-view/auth-check-view.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    AuthRoutingModule
  ],
  declarations: [
    AuthViewComponent,
    AuthLoginViewComponent,
    AuthCheckViewComponent
  ]
})
export class AuthModule { }
