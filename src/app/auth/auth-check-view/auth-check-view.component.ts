import { Component } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { throwError, of } from 'rxjs';
import { environment } from '@env/environment';
import { FormBuilder } from '@angular/forms';
import { RouteService, StorageService, AlertService } from '@squarepanda/sqp-core';
import { SqpCountriesService } from '@squarepanda/sqp-reducer';
import { UsersApiService } from '@squarepanda/sqp-api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth-check-view',
  templateUrl: './auth-check-view.component.html',
  styleUrls: ['./auth-check-view.component.scss']
})
export class AuthCheckViewComponent {

  form;
  pressed;
  countries$;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private alert: AlertService,
    private route: RouteService,
    private storage: StorageService,
    private usersApi: UsersApiService,
    private sqpCountries: SqpCountriesService
  ) {
    this.countries$ = this.sqpCountries.get();

    this.form = this.fb.group({
      phoneNumber: this.fb.group({
        countryCode: [environment.countryCode],
        number: []
      })
    });

    this.storage.clear();
  }

  submit(body) {
    if (!this.pressed) {
      this.pressed = true;
      this.check(body).pipe(
        tap((response) => {
          if (!response.userExists && !response.phoneVerified) {
            this.pressed = false;
            this.alert.info({ type: 'fail', message: 'user.status.invalid' });
            return;
          }

          if (response.userExists && !response.phoneVerified) {
            this.route.go('auth.otp');
            return;
          }

          if (response.userExists) {
            this.router.navigate(['auth/login'], {
              queryParams: {
                number: body.phoneNumber.number,
                countryCode: body.phoneNumber.countryCode
              }
            });
            return;
          }

        })
      ).subscribe();
    }
  }

  login() {
    this.route.go('auth.login');
  }

  password() {
    this.route.go('auth.forgotpassword');
  }

  private check(params) {
    return this.usersApi.postUsersVerify(params).pipe(
      catchError((err) => {
        this.pressed = false;
        return throwError(err);
      })
    );
  }

}
