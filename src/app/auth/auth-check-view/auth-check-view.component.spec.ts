import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthCheckViewComponent } from './auth-check-view.component';

describe('AuthCheckViewComponent', () => {
  let component: AuthCheckViewComponent;
  let fixture: ComponentFixture<AuthCheckViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthCheckViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthCheckViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
