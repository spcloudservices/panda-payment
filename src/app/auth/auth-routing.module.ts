import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthViewComponent } from './auth-view/auth-view.component';
import { AuthLoginViewComponent } from './auth-login-view/auth-login-view.component';
import { AuthCheckViewComponent } from './auth-check-view/auth-check-view.component';

const routes: Routes = [
  {
    path: '',
    component: AuthViewComponent,
    children: [
      { path: '', component: AuthCheckViewComponent },
      { path: 'login', component: AuthLoginViewComponent },
      // { path: 'user', loadChildren: 'app/user/user.module#UserModule' }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
