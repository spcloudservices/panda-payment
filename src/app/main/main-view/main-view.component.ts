import { Component, OnInit } from '@angular/core';
import { StripeService } from '@shared/_services/stripe.service';
import { FormBuilder, Validators } from '@angular/forms';
import { tap, switchMap, filter } from 'rxjs/operators';
import { SqpSdkAuthService, SqpSdkIdentityService, SqpSdkStudentService } from '@squarepanda/sqp-sdk';
import { OrdersApiService, UsersApiService } from '@squarepanda/sqp-api';
import { SqpIdentityService } from '@squarepanda/sqp-reducer';
import { SqpStudentsService } from '@squarepanda/sqp-selector';
import { ActivatedRoute } from '@angular/router';
import { SQP_GENDER_CHOICES } from '@squarepanda/sqp-constants';
import { environment } from '@env/environment';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit {

  purchaseForm;
  form;
  studentForm;
  cardElement;
  students$;
  identity$;
  options = {
    tos: environment.CON_termsContentCode,
    pp: environment.CON_privacyContentCode
  };

  constructor(
    private fb: FormBuilder,
    private stripe: StripeService,
    private usersApi: UsersApiService,
    private sqpAuth: SqpSdkAuthService,
    private ordersApi: OrdersApiService,
    private activatedRoute: ActivatedRoute,
    private sqpStudents: SqpStudentsService,
    private sqpIdentity: SqpIdentityService,
    private sdkStudent: SqpSdkStudentService
  ) {

    this.purchaseForm = this.fb.group({
      tierId: [null, [Validators.required]],
      studentId: [null, [Validators.required]],
      pricingId: [null, [Validators.required]],
      callerId: ['paymentPortal', [Validators.required]],
      organizationId: [null, [Validators.required]],
      paymentMethod: this.fb.group({
        type: ['Stripe', [Validators.required]]
      })
    });

    this.studentForm = this.fb.group({
      firstName: [null, [Validators.required]],
      dateOfBirth: ['2014-10-10'],
      gender: [SQP_GENDER_CHOICES[0].id, [Validators.required]]
    });

    this.purchaseForm.valueChanges.pipe(
      filter(() => this.purchaseForm.valid),
      switchMap(() => this.purchase(this.purchaseForm.value))
    ).subscribe();

    this.activatedRoute.queryParams.pipe(
      tap((params) => {
        if (params.sku) {
          const skus = {
            'SPBR-0003-CN': { tierId: 19855, pricingId: 5586 }
          };
          const sku = skus['SPBR-0003-CN'];
          this.purchaseForm.controls['tierId'].patchValue(sku.tierId);
          this.purchaseForm.controls['pricingId'].patchValue(sku.pricingId);
        }
      }),
    ).subscribe();

    this.form = this.fb.group({
      billing_details: this.fb.group({
        email: ['philip.lim+address2@squarepanda.com'],
        name: 'philip'
      })
    });

    this.students$ = this.sqpStudents.get();

    this.identity$ = this.sqpIdentity.get().pipe(
      tap((user) => this.purchaseForm.controls['organizationId'].patchValue(user.affiliation.organization.id))
    );
  }

  ngOnInit() {
    this.stripe.register('pk_test_8OplOrXg99XBLjB1pR3KT1lq00LeUXQgLU');
  }

  step1() {
    const elements = this.stripe.elements();

    // Set up Stripe.js and Elements to use in checkout form
    const style = {
      base: {
        color: "#32325d",
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#aab7c4"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    };

    this.cardElement = elements.create('card', { style: style });
    this.cardElement.mount('#card-element');

    this.cardElement.addEventListener('change', function (event) {
      const displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });
  }

  step2(billing_details) {
    const body = { emailAddress: billing_details.email, password: 'Testing123' };
    return this.login(body).pipe(
      switchMap(() => this.sdkStudent.getStudents())
    ).subscribe();
  }

  step3() {

  }

  selectItem(student) {
    this.purchaseForm.controls['studentId'].patchValue(student.id);
  }

  deleteItem(student) {
    this.sdkStudent.deleteStudent(student.id).subscribe();
  }

  createStudent(affiliation, studentForm) {
    const body = Object.assign({}, studentForm, {
      organizationId: affiliation.organization.id,
      organizationLocationId: affiliation.organizationLocation.id,
      classroomId: affiliation.classroom.id
    });
    return this.sdkStudent.postStudents(body).pipe(
      tap(console.log)
    ).subscribe();
  }

  private login(params) {
    return this.sqpAuth.loginV2(params).pipe(
      switchMap((response) => {
        const userId = response.body.id;
        return this.usersApi.getUsersId(userId).pipe(
          tap((user) => this.sqpIdentity.set(user)),
          switchMap(() => this.sqpIdentity.get())
        );
      })
    );
  }

  private purchase(purchaseOrder) {
    return this.ordersApi.postOrders(purchaseOrder);
  }

}
