import { Injectable } from '@angular/core';
import { OrdersApiService } from '@squarepanda/sqp-api';
import { from } from 'rxjs';
declare var Stripe: any;

@Injectable({
  providedIn: 'root'
})
export class StripeService {

  private stripe;

  constructor(private ordersApi: OrdersApiService) { }

  register(stripeKey) {
    this.stripe = Stripe(stripeKey);
  }

  elements() {
    return this.stripe.elements();
  }

  createPaymentMethod(cardElement, billing_details?) {
    return from(this.stripe.createPaymentMethod('card', cardElement, { billing_details }));
  }



}
