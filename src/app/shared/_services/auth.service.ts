import { Injectable, Inject } from '@angular/core';
import { UsersApiService } from '@squarepanda/sqp-api';
import { DOCUMENT } from '@angular/common';
import { switchMap, filter, tap, catchError } from 'rxjs/operators';
import { of, combineLatest, throwError } from 'rxjs';
import { SqpIdentityService, SqpWorkflowService } from '@squarepanda/sqp-reducer';
import { environment } from '@env/environment';
import { RouteService, StorageService } from '@squarepanda/sqp-core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private route: RouteService,
    private storage: StorageService,
    private usersApi: UsersApiService,
    @Inject(DOCUMENT) private document,
    private sqpWorkflow: SqpWorkflowService,
    private sqpIdentity: SqpIdentityService
  ) {
  }

  // v3/login response
  continue(params: { id: string; }) {
  }

}
