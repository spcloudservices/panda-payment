import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SqpAnalyticModule } from '@squarepanda/sqp-analytic';
import { SqpCommonModule } from '@squarepanda/sqp-common';
import { SqpCoreModule } from '@squarepanda/sqp-core';
import { SqpUiModule } from '@squarepanda/sqp-ui';
import { HeaderComponent } from './_components/header/header.component';
import { SqpReducerModule } from '@squarepanda/sqp-reducer';
import { SqpStudentModule } from '@squarepanda/sqp-student';

const modules = [
  ReactiveFormsModule,
  SqpAnalyticModule,
  SqpCommonModule,
  SqpCoreModule,
  SqpUiModule,
  SqpStudentModule,
  SqpReducerModule
];

@NgModule({
  imports: [
    CommonModule,
    ...modules
  ],
  declarations: [
    HeaderComponent
  ],
  exports: [
    ...modules,
    HeaderComponent,
  ]
})
export class SharedModule { }
