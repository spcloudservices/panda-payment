import { Component, Output, EventEmitter } from '@angular/core';
import { ModalService, RouteService } from '@squarepanda/sqp-core';
import { SqpIdentityService } from '@squarepanda/sqp-reducer';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent {

  @Output() toggleMenu = new EventEmitter;
  identity$;
  menu$;

  constructor(
    private route: RouteService,
    private modal: ModalService,
    private sqpIdentity: SqpIdentityService
  ) {
    this.identity$ = this.sqpIdentity.get();
  }

}
